import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  string, shape, func, arrayOf,
} from 'prop-types';
import forkingImage from 'images/forking.png';
import { OPERATION, ALGORITHM, VISUALIZATION } from 'dataTypes';
import { toastr } from 'react-redux-toastr';
import FilesContainer from 'components/FilesContainer';
import { filterSetsBy } from 'functions/dataParserHelpers';
import ReadMeComponent from '../ReadMe/ReadMe';
import ProjectContainer from '../projectContainer';
import RepoInfo from '../repoInfo';
import RepoFeatures from '../repoFeatures';
import Navbar from '../navbar/navbar';
import * as projectActions from '../../actions/projectInfoActions';
import * as branchesActions from '../../actions/branchesActions';
import * as processorActions from '../../actions/processorActions';
import './projectView.css';
import * as userActions from '../../actions/userActions';
import * as jobsActions from '../../actions/jobsActions';
import * as mergeActions from '../../actions/mergeActions';
import EmptyProject from './emptyProject';
import ProjectLastCommitSect from './projectLastCommitSect';

const isValidBranch = (branch) => branch !== 'null' && branch !== null && branch !== undefined;

class ProjectView extends React.Component {
  constructor(props) {
    super(props);
    // esLint does not like state out of constructor
    this.state = {
      contributors: [], // disconnected
      isForking: false,
    };
    this.setIsForking = this.setIsForking.bind(this);
    this.fetchIfAuthenticated = this.fetchIfAuthenticated.bind(this);
    this.fetchVisitor = this.fetchVisitor.bind(this);
  }

  componentDidMount() {
    const {
      actions,
      user: { auth },
    } = this.props;

    const fetch = auth ? this.fetchIfAuthenticated : this.fetchVisitor;

    actions.setIsLoading(true);

    fetch().finally(() => actions.setIsLoading(false));
  }

  setIsForking(status) {
    this.setState({ isForking: status });
  }

  fetchVisitor() {
    const {
      actions,
      match: {
        params: {
          namespace,
          slug,
        },
      },
    } = this.props;

    return actions.getProjectDetailsBySlug(namespace, slug, { visitor: true })
      .then(({ project }) => {
        const gid = project.gitlabId || project.gitlab?.id;
        return Promise.all([
          actions.getBranchesList(gid),
          actions.getMergeRequestsList(gid),
          actions.getUsersList(gid),
          actions.getProjectStarrers(gid),
        ]);
      })
      .catch(() => toastr.error('Error', 'Error fetching project'));
  }

  fetchIfAuthenticated() {
    const {
      actions,
      match: {
        params: {
          namespace,
          slug,
        },
      },
    } = this.props;

    return actions.getProjectDetailsBySlug(namespace, slug)
      .then(({ project }) => {
        const gid = project.gitlabId || project.gitlab?.id;

        actions.getProcessors(OPERATION);
        actions.getProcessors(ALGORITHM);
        actions.getProcessors(VISUALIZATION);

        return Promise.all([
          actions.getBranchesList(gid),
          actions.getMergeRequestsList(gid),
          actions.getUsersList(gid),
          actions.getJobsListPerProject(gid),
          actions.getProjectStarrers(gid),
        ]);
      })
      .catch(() => toastr.error('Error', 'Error fetching project'));
  }

  render() {
    const {
      project,
      project: {
        gid,
        httpUrlToRepo,
        readmeUrl: showReadMe,
      },
      match: {
        params: {
          namespace,
          slug,
          path,
          branch,
        },
      },
      mergeRequests,
      branches,
      users,
    } = this.props;
    const {
      contributors,
      isForking,
    } = this.state;

    const currentBranch = isValidBranch(branch) ? branch : project.defaultBranch;
    const decodedBranch = decodeURIComponent(currentBranch);

    return (
      <div className="project-component">
        <Navbar />
        {isForking && (
          <div
            className="mx-auto mt-5 t-center"
            style={{ maxWidth: '250px' }}
          >
            <div>
              <h2 className="t-dark">Forking in progress</h2>
              <p className="t-secondary">You may wait while we import the repository for you. You may refresh at will.</p>
            </div>
            <div
              className="bg-image m-auto"
              style={{
                backgroundImage: `url(${forkingImage})`,
                width: '200px',
                height: '160px',
              }}
            />
          </div>
        )}
        <div style={{ display: isForking ? 'none' : 'block' }}>
          {gid && (
            <ProjectContainer
              setIsForking={this.setIsForking}
              activeFeature="data"
            />
          )}
          {gid && (
          <div className="main-content">
            {project.emptyRepo ? (
              <EmptyProject httpUrlToRepo={httpUrlToRepo} projectId={gid} />
            ) : (
              <>
                <RepoInfo
                  project={project}
                  mergeRequests={mergeRequests}
                  currentBranch={decodedBranch}
                  numberOfContributors={contributors.length}
                  branchesCount={branches.length}
                  visualizationsCount={filterSetsBy(branches)('visualization').length}
                  dataInstanesCount={filterSetsBy(branches)('pipeline').length}
                />
                <ProjectLastCommitSect
                  projectId={gid}
                  branch={currentBranch}
                  projectDefaultBranch={project.defaultBranch}
                  users={users}
                />
                <RepoFeatures
                  projectId={gid}
                  branch={decodedBranch}
                  path={path || ''}
                  searchableType={project.searchableType}
                />
                <FilesContainer
                  projectId={gid}
                  namespace={namespace}
                  slug={slug}
                  path={path}
                  urlBranch={currentBranch}
                  defaultBranch={project.defaultBranch}
                />
                {showReadMe && (
                <ReadMeComponent
                  projectId={gid}
                  branch={decodedBranch}
                />
                )}
              </>
            )}
          </div>
          )}
        </div>
      </div>
    );
  }
}

ProjectView.defaultProps = {
  mergeRequests: [],
};

ProjectView.propTypes = {
  project: shape({}).isRequired,
  mergeRequests: arrayOf(shape({})),
  projects: shape({
    all: arrayOf.isRequired,
  }).isRequired,
  match: shape({
    params: shape({
      namespace: string.isRequired,
      slug: string.isRequired,
      file: string,
      branch: string,
      path: string,
    }),
  }).isRequired,
  users: arrayOf(shape({
    name: string.isRequired,
  })).isRequired,
  branches: arrayOf(
    shape({
    }).isRequired,
  ).isRequired,
  actions: shape({
    setSelectedProject: func.isRequired,
    getUsersList: func.isRequired,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    projects: state.projects,
    users: state.users,
    user: state.user,
    branches: state.branches,
    mergeRequests: state.mergeRequests.list,
    project: state.projects.selectedProject,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      ...projectActions,
      ...userActions,
      ...jobsActions,
      ...branchesActions,
      ...mergeActions,
      ...processorActions,
    }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectView);
