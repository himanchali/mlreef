import React from 'react';
import './MLoadingSpinner.scss';

/* This comp can be provisional, it's intended to get rid of mat-ui */

export default () => <div className="m-loading-spinner waiting" />;
