# Explore

## Routes
+ `/explore`

## Development notes
+ This view is functional simplified version of *MyProjects* suited for visitors.

## TODO
+ Add the loading gif.
+ Check if *dataTypes* suits with the backend requirements.
+ Complete filtering logic.
+ Adapt the colored bar (currently not changing color).
