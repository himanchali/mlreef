import ApiDirector from './ApiDirector';
import BodyLessApiRequestCallBuilder from './apiBuilders/BLApiRequestCallBuilder';
import { METHODS, validServicesToCall } from './apiBuilders/requestEnums';
import { handleResponse } from 'functions/apiCalls';

export default class JobsApi extends ApiDirector {
  getPerProject(projectId: number) {
    const url = `/api/v4/projects/${projectId}/jobs`;
    const blbuilder = new BodyLessApiRequestCallBuilder(METHODS.GET, this.buildBasicHeaders(validServicesToCall.GITLAB), url);

    return fetch(blbuilder.build())
      .then(handleResponse);
  }

  getJobById(projectId: number, jobId: number) {
    const url = `/api/v4/projects/${projectId}/jobs/${jobId}`;
    const blbuilder = new BodyLessApiRequestCallBuilder(METHODS.GET, this.buildBasicHeaders(validServicesToCall.GITLAB), url);

    return fetch(blbuilder.build())
      .then(handleResponse);    
  }

  downloadArtifacts(projectId: number, refName: string, jobName: string) {
    const url = `/api/v4/projects/${projectId}/jobs/artifacts/${refName}/download?job=${jobName}`;
    const blbuilder = new BodyLessApiRequestCallBuilder(METHODS.GET, this.buildBasicHeaders(validServicesToCall.GITLAB), url);

    return fetch(blbuilder.build())
      .then(handleResponse);    
  }

  getLog(projectId: number, jobId: number) {
    const url = `/api/v4/projects/${projectId}/jobs/${jobId}/trace`;
    const blbuilder = new BodyLessApiRequestCallBuilder(METHODS.GET, this.buildBasicHeaders(validServicesToCall.GITLAB), url);

    return fetch(blbuilder.build());
  }
}
