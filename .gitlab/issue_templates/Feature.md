[MLReef's Glossary](https://gitlab.com/mlreef/www-mlreef-com/-/tree/master/handbook/glossary.md)

### Problem to solve
> Describe the desired state of the system in the style of:
> “As a (User / Manager / Admin / Guest) I want to be able to do x”


### Intended users


### User experience goal
 

### Proposal for Technical Solution
> Add technical implementation details and the results of the ticket's discussion here.


### Permissions and Security


### Documentation


### Availability & Testing


### What does success look like, and how can we measure that?



Additional Notes
=====================
### What is the type of buyer?


### Is this a cross-stage feature?


### Links / references
