#!/bin/bash
# change to the repository root folder via the scripts location
cd "$(dirname "$0")"/..
. bin/includes/log
. bin/includes/detect-os
. bin/includes/detect-docker-runtime
. bin/includes/gitlab-health
. bin/includes/install-parse-cli-params
########################################
set -x  # output all commands
set -o  pipefail
set -e  # exit on immediately on every error
set -u  # error on usage of undefined variables
########################################

log "###### DOCKER_NVIDIA_RUNTIME $DOCKER_NVIDIA_RUNTIME"

export GITLAB_PORT=10080

# backup local.env if it exits
cp local.env local.env.bak 2>/dev/null || true
# delete local.enc if it exists
rm -f local.env 2>/dev/null || true
###############################################################################

. bin/includes/install-render-local-env


####
log "Starting Deployment"
if [ "$INSTANCE_HOST" != "localhost" ]; then
  # Cleanup docker images and volumes
  . bin/includes/install-cleanup-docker
  # Dowload all needed images
  docker-compose -f docker-compose.local.yml pull
fi
log "Stopping service gateway backend mlreefdb"
docker-compose -f docker-compose.local.yml stop gateway backend mlreefdb
log "Starting Deployment"

####
log "MANDATORY ENV VARS:"
cat local.env


#
#
# Step 1 Startup Gitlab
#
#
log "1. Starting Gitlab Omnibus"
# the container ec2-startup wait is necessary to let gitlab initialise the database
log "docker-compose up -f docker-compose.local.yml --detach gitlab gitlab-runner"
docker-compose -f docker-compose.local.yml up --detach gitlab gitlab-runner
waitUntilGitlabAvailable

#
#
# Step 2 Configure Gitlab's port and external URL
#
#
if [ "$(checkGitlabPort)" = "302" ]; then
  log "2. Found Gitlab at expected port $GITLAB_PORT. Printing relevant parts of /etc/gitlab/gitlab.rb"
  docker exec gitlab sh -c 'cat /etc/gitlab/gitlab.rb | grep external_url\ \"'
elif [ "$(checkGitlab80)" = "302" ]; then
  log "Found Gitlab at port 80. Reconfiguring Gitlab for port $GITLAB_PORT"
  if [ "$INSTANCE_HOST" != "localhost" ]; then
    log "2. Configure Gitlab external_url 'http://$INSTANCE_HOST:$GITLAB_PORT'"
    echo "external_url \"http://${INSTANCE_HOST}:${GITLAB_PORT}\"" | docker exec -i gitlab sh -c "cat >> /etc/gitlab/gitlab.rb"
  else
    log "2. Configure Gitlab external_url 'http://gitlab:$GITLAB_PORT'"
    # When running locally in docker compose, this lets the runners access Gitlab via the Docker network
    echo "external_url \"http://gitlab:${GITLAB_PORT}\"" | docker exec -i gitlab sh -c "cat >> /etc/gitlab/gitlab.rb"
  fi
  log "Printing new Gitlab external_url configuration"
  docker exec gitlab sh -c 'tail -1 /etc/gitlab/gitlab.rb'
  log "Reconfigure Gitlab"
  docker exec gitlab gitlab-ctl reconfigure > /dev/null
  log "Restart Gitlab"
  docker exec --detach gitlab gitlab-ctl restart
  sleep 30
  log "Waiting for Gitlab to start"
  until [ "$(checkGitlabPort)" = "302" ]; do
    printf '.'
    sleep 5;
  done
  log "Expecting code 302; received: $(checkGitlabPort)"
fi

log "Ensuring availability of the Gitlab API to start"
until [ "$(checkGitlabPort /api/v4/projects)" = "200" ]; do
  printf '.'
  sleep 5;
done
log "Expecting code 200; received: $(checkGitlabPort /api/v4/projects)"
log "Waiting for Gitlab Runners API. The runners API is running in a separate process from the normal API"
until [ "$(checkGitlabPort /runners)" = "302" ]; do
  printf '.'
  sleep 5;
done
log "Expecting code 302; received: $(checkGitlabPort /runners)"

#
#
# Step 3 Creating Gitlab Admin API token
#
#
log "3. Deleting all API tokens for root user (id=1)"
# http://gitlab.com/help/administration/troubleshooting/gitlab_rails_cheat_sheet.md
# Alternatively the token digest can be computed as follows:
# salt=$(echo $GITLAB_SECRETS_DB_KEY_BASE | cut -c1-32)
# token=$GITLAB_ADMIN_TOKEN$salt
# token_digest=$(echo $token | openssl sha256 -binary | base64 -)
docker exec -t gitlab sh -c "$(cat << EOM
  gitlab-rails runner -e production "
    User.find(1).personal_access_tokens.each do |cur|
      cur.delete
    end
  "
EOM
)"
log "3. Creating Admin API token $GITLAB_ADMIN_TOKEN. This might take up to 5 minutes"
docker exec -t gitlab sh -c "$(cat << EOM
  gitlab-rails runner -e production "User.find(1).personal_access_tokens.create(
    name: 'admin-api-token',
    token_digest: Gitlab::CryptoHelper.sha256('$GITLAB_ADMIN_TOKEN'),
    impersonation: false,
    scopes: [:api,:sudo]
  )"
EOM
)" #end of $(cat …)


#
#
# Step 4 Get Gitlab Runner registration token
#
#
log "4. Getting Gitlab runners registration token from Gitlab."
RUNNER_REGISTRATION_TOKEN=$(docker exec -t gitlab bash -c 'gitlab-rails runner -e production "puts Gitlab::CurrentSettings.current_application_settings.runners_registration_token"' | tr -d '\r')
echo "Gitlab RUNNER_REGISTRATION_TOKEN=$RUNNER_REGISTRATION_TOKEN"

#
#
# Step 5 Register Gitlab Runner
#
#
if [ "$INSTANCE_HOST" != "localhost" ]; then
  log "5. Configuring Gitlab Runner for cloud environment"
#  log "5.1 unregister current runners"
#  docker exec -t gitlab-runner sh -c "$(cat << EOM
#    cat /etc/gitlab-runner/config.toml            \
#      | grep "token ="                            \
#      | sed "s/token//"                           \
#      | tr -d " \t"                               \
#      | tr -d "\"="                               \
#      | xargs --no-run-if-empty -I {}             \
#        gitlab-runner unregister                  \
#        --url="http://gitlab:10080/"              \
#        --token={}
#EOM
#  )" #end of $(cat …)

  CONFIG_TEMPLATE="/etc/gitlab-runner/runner-config.template.toml"
  docker cp bin/resources/runner-config.template.toml gitlab-runner:etc/gitlab-runner/
  docker cp bin/resources/development.pem             gitlab-runner:etc/gitlab-runner/
  docker cp bin/resources/development.pem.pub         gitlab-runner:etc/gitlab-runner/
  #openssl rsa -pubout -in bin/resources/development.pem  > bin/resources/development.pem.pub
  docker exec gitlab-runner sed -i "s/###AWS_ACCESS_KEY###/$AWS_ACCESS_KEY_ID/"        $CONFIG_TEMPLATE
  docker exec gitlab-runner sed -i "s~###AWS_ACCESS_SECRET###~$AWS_SECRET_ACCESS_KEY~" $CONFIG_TEMPLATE

  # clear runner config
  docker exec gitlab-runner sh -c "echo '' > /etc/gitlab-runner/config.toml"

  sleep 10 # This sleep is necessary to allow the gitlab Runner to reload the now empty config file

  # https://docs.gitlab.com/runner/configuration/advanced-configuration.html#volumes-in-the-runnersdocker-section
  docker exec gitlab-runner gitlab-runner register                \
    --non-interactive                                             \
    --template-config="$CONFIG_TEMPLATE"                          \
    --name="Multi runner dispatcher on $INSTANCE_HOST"                 \
    --url="http://$INSTANCE_HOST:$GITLAB_PORT/"                        \
    --registration-token="$RUNNER_REGISTRATION_TOKEN"             \
    --request-concurrency="12"                                    \
    --executor="docker+machine"                                   \
    --docker-image="alpine:latest"                                \
    --tag-list="docker"                                           \
    --run-untagged="true"                                         \
    --locked="false"                                              \
    --access-level="not_protected"                                \
    --cache-s3-access-key="$AWS_ACCESS_KEY_ID"                    \
    --cache-s3-secret-key="$AWS_SECRET_ACCESS_KEY"                \

else ### Configuration for local environments ###
  log "5. Configuring Gitlab Runner for local environment"
  # Register the runner on a local developers machine
  # The main differences are the URL and,
  # no caching on Amazon S3 buckets
  docker exec gitlab-runner gitlab-runner register                \
    --non-interactive                                             \
    --name="Packaged Dispatcher on $INSTANCE_HOST"                     \
    --url="http://gitlab:$GITLAB_PORT/"                           \
    --docker-network-mode mlreef-docker-network                   \
    --registration-token="$RUNNER_REGISTRATION_TOKEN"             \
    --executor "docker"                                           \
    --docker-image alpine:latest                                  \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock    \
    --tag-list "docker"                                           \
    --run-untagged="true"                                         \
    --locked="false"                                              \
    --access-level="not_protected"
fi
log "Runner was registered successfully"

#
#
# Step 6 Start other Services
#
#
log "Ensuring availability of the Gitlab API to start"
until [ "$(checkGitlabPort /api/v4/projects)" = "200" ]; do
  printf '.'
  sleep 5;
done
log "Expecting code 200; received: $(checkGitlabPort /api/v4/projects)"

log "6. Start other services"
docker-compose -f docker-compose.local.yml up --detach redis mlreefdb
if [ "${SKIP_BACKEND_DEPLOYMENT:-false}" != "true" ]; then
  docker-compose -f docker-compose.local.yml up --detach backend
fi
if [ "${SKIP_GATEWAY_DEPLOYMENT:-false}" != "true" ]; then
  docker-compose -f docker-compose.local.yml up --detach gateway
fi
sleep 30 # Add an additional sleep in the end to improve user experience; So that Docker is started when the script ends

#
echo "Debug Log: gitlab runner configuration"
docker exec gitlab-runner cat /etc/gitlab-runner/config.toml

log "Done - MLReef has been successfully installed. "
#
#echo Test connection for admin:
#curl -f -I -X GET --header "Content-Type: application/json" --header "Accept: application/json" --header "PRIVATE-TOKEN: $GITLAB_ADMIN_TOKEN" "localhost:20080/api/v1"
#curl -f -I -X GET --header "Content-Type: application/json" --header "Accept: application/json" --header "PRIVATE-TOKEN: $GITLAB_ADMIN_TOKEN" "localhost:10080/api/v4/users/1"
